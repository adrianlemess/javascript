Aula 1
* Introdução
    * Apresentações
    * O que eles tiveram até agora em DW1
    * O que tiveram de disciplinas (Sabem REST?)
    * Trello e objetivos da disciplina
    * Setup (HTML snippets, Auto Import, Color Picker, Debugger for Chrome, ES7 React snippets, eslint, git blame, git history, git lens, IntelliSense for css, Material Icon Theme, Open in Browser, Prettier, TODO Highlight, SASS)
    * Devtools
    * Flexbox
* Introdução JavaScript
    * História
    * O que é
    * Conceitos
    * Como rodar JavaScript




Como rodar JavaScript
* no body
<body>
    <script>
        setTimeout(function() {
            alert('Testando JavaScript');
        }, 3000)
    </script>
</body>

* no head
<head>
    <script>
        setTimeout(function() {
            console.log('Testando JavaScript');
        }, 3000)
    </script>
</head>

* Em um tag
<body>
    <input type="button" value="Clique aqui" onclick="alert('Voce clicou no input')"> 
</body>

* Em um arquivo separado
<script type="text/javascript" src="script01.js"></script>


Mostrar conceitos básicos

* JavaScript é case sensitive
* Tipagem dinâmica
    * Mostrar todos os tipos de valores (int, float, string, nulo, undefined, array e objeto)
    * Mostrar que uma variável por ser sobrescrita
* Comentários (bloco e linha)
* Operadores aritméticos
* Operadores de atribuição
* Operadores de comparação
* Objetos dinâmicos



Acessando propriedades
* Mostrar exemplo do nome-pet no console

